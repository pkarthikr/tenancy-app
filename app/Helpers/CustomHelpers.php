<?php 

namespace App\Helpers;

class ImageFunctions
{
	//Uploads the image and returns the file name
    public static function uploadImage($file, $location){
     $ext = $file->getClientOriginalExtension();
     $uploadPath = $location;
     $get_name = $file->getClientOriginalName();
     $fileName = rand().\Carbon\Carbon::now()->timestamp. '.'.$ext;
     $upload=$file->move($uploadPath,$fileName);
     return $fileName;
    }
}