<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Period;
use Carbon\Carbon;
use App\Tenant;
use App\PeriodTenant;

class PeriodsController extends Controller
{
	//Returns the view for adding a tenancy period
    public function create($id){
    	//To Do : Return Property Name
    	return view('periods.create', compact('id'));
    }

    //Assign a tenant to this tenancy
    public function assign($id=null){
        //If approached using GET Request, Put manually in session
        if($id != null)
        {
           \Session::put('tenancy_id',$id); 
        }

        $tenants = Tenant::leftJoin('period_tenant',function($join) use($id){
                $join->on('period_tenant.tenant_id','=','tenants.id')
                    ->where('period_tenant.tenant_id','=',$id);
        })->whereNull('period_tenant.tenant_id')
            ->select('*')
            ->get();

        // dd($tenants);

        $tenants = Tenant::all();
        return view('periods.assign',compact('tenants'));
    }

    public function addTenants(Request $request){

        $userInputs = $request->all();
    
        if(!isset($userInputs['tenant']) && count($userInputs['tenant']) == 0)
        {
            return redirect()->back()->withInputs()->withErrors();
        }

        foreach($userInputs['tenant'] as $key=>$value)
        {
            $data[$key]['tenant_id'] = $value; 
            $data[$key]['period_id'] = $userInputs['tenancy'];
        }

        PeriodTenant::insert($data);
        return redirect('/properties');
        // $newTenant = new PeriodTenant;

    }


    //Store a tenancy here.
    public function add(Request $request){
    	//Validate the request. 
    	//TO Do : Check if Start Date > End Date
        $messages['end_date.greater_than'] = 'End date should be greater than Start Date';

    	$this->validate($request, [
        'start_date' => 'required',
        'end_date'=>'required|greater_than:start_date',
        'monthly_rent'=>'numeric'
         ],$messages);

    	//Adds the tenancy if validation passes
    	$tenancy = new Period;
    	$tenancy->start_date = Carbon::parse($request['start_date'])->format('Y-m-d');
    	$tenancy->end_date = Carbon::parse($request['end_date'])->format('Y-m-d');;
    	$tenancy->monthly_rent = $request->input('monthly_rent');
    	$tenancy->property_id = $request->input('property_id');
    	$tenancy->save();
    	
        $tenancy_id = $tenancy->id;
        
        return redirect('/tenancy/assign')->with('tenancy_id',$tenancy_id);

        // $request->session()->flash('alert-success','Tenancy added successfully');


        // return redirect('/properties');
    }
}
