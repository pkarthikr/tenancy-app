<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Property;
use App\PeriodTenant;

class PropertiesController extends Controller
{
	//Returns the dashboard of properties
    public function index(){
    	$properties = Property::paginate(10);
    	return view('properties.index',compact('properties'));
    }

    //Displays the page for creating a Property
    public function create(){
    	return view('properties.create');
    }

    //View Properties Controller
    public function view($id){
        $property = Property::with('period')->findOrFail($id);
        $periods = $property->period;
        $periodData = array();
        foreach($periods as $key=>$value)
        {
            $periodData[$key] = $value->toArray();
            $count = PeriodTenant::where('period_id',$value->id)->count();
            $periodData[$key]['count'] = $count;
        }
        return view('properties.view',compact('periodData','property'));
    }


    //Adds the property to the database after validating
    public function add(Request $request){
    	//Validate the request
    	$this->validate($request, [
        'name' => 'required|max:45',
        'address'=>'required',
        'property_value'=>'required|numeric',
        'mortgage_value'=>'numeric'
         ]);

        //Uploads the image and returns the file name
        $img_filename = \ImageFunctions::uploadImage($request->file('image'),'properties-images/');

    	//If validation is successful, add the property
        $property = new Property;
        $property->name = $request->input('name');
        $property->address = $request->input('address');
        $property->property_value = $request->input('property_value');
        $property->mortgage_value = $request->input('mortgage_value');
        $property->image_path = $img_filename;
        $property->save();

        $request->session()->flash('alert-success', 'Property added successfully!');

        return redirect('/properties');
    }
}
