<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Tenant;

class TenantsController extends Controller
{
	//Returns the dashboard of properties
    public function index(){
    	$tenants = Tenant::paginate(10);
    	return view('tenants.index',compact('tenants'));
    }

	//Create Tenants
    public function create(){
    	return view('tenants.create');
    }

    //Add Tenants 
    public function add(Request $request){
    	//Validate the request
    	$this->validate($request, [
        'name' => 'required|max:45',
        'address'=>'required'
         ]);

        $img_filename = \ImageFunctions::uploadImage($request->file('image'),'user-avatars/');

    	$tenant = new Tenant;
    	$tenant->name = $request->input('name');
    	$tenant->address = $request->input('address');
        $tenant->image = $img_filename;
    	$tenant->save();
        $request->session()->flash('alert-success', 'Tenant added successfully!');

        return redirect('/tenants');

    }
}
