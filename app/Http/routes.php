<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::group(['middleware'=>['auth']],function(){
	Route::get('/properties', 'PropertiesController@index');
	Route::get('/properties/create', 'PropertiesController@create');
	Route::post('/properties/add', 'PropertiesController@add');
	Route::get('/properties/{id}', 'PropertiesController@view');

	Route::get('/tenancy/create/{id}','PeriodsController@create');
	Route::post('/tenancy/add', 'PeriodsController@add');
	Route::get('/tenancy/assign', 'PeriodsController@assign');
	Route::post('/tenancy/done', 'PeriodsController@addTenants');
	Route::get('/tenancy/assign/{id}', 'PeriodsController@assign');
	
	Route::get('/tenants', 'TenantsController@index');
	Route::get('/tenants/create', 'TenantsController@create');
	Route::post('/tenants/add', 'TenantsController@add');
});
