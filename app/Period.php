<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
	protected $table = 'periods';

    public function tenants(){
    	return $this->hasMany('App\Tenant');
    }

    public function periodtenants()
    {
    	return $this->hasMany('App\PeriodTenant','period_tenant','period_id');
    }
}
