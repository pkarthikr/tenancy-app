<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeriodTenant extends Model
{
    protected $table = 'period_tenant';
    protected $guarded = ['id'];

}
