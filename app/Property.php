<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    public function period()
    {
    	return $this->hasMany('App\Period');
    }

    public function periodtenants()
    {
    	return $this->hasMany('App\PeriodTenant');
    }
}
