<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Requests\Request;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //greater than rule is added to check whether one field is greater than other
        \Validator::extend('greater_than', function($attribute, $value, $parameters)
        {
            $other = \Input::get($parameters[0]);
            $end_date = $this->app->request->end_date;
            $start_date = $this->app->request->start_date;
            // $pos = array_search($value, $end_date);
            $other = $start_date;
            return isset($other) and intval($value) >= intval($other);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
