Task Description : 

Please set up a new L5.2 project. Create a user admin/login system. Create a ‘properties’ and ‘tenants’ table/model. Allow logged in user to create properties, enter standard info like name, address, property value and mortgage

Logged in user can create a tenancy, and assign it to a property. Tenancy includes start date, end date, monthly rent.

Logged in user can create tenants (name, address) and assign one or more tenants to a tenancy.

Front end should just be basic bootstrap

I should be able to unzip the project, run composer install, edit .env, run php artisan migrate, and project should work

--


The login system will have Laravel's basic auth scaffolding treatment. 

We will start with the basic properties model : 

A Property will have the following features about it
Name
Address 
Property Value 
Mortgage Value 
Cover Image for the Property 

We will make one main screen : 

Properties Main Page : 

This page will list all the properties added by the current user. And at the top, the user will have a search bar which will allow filtering the user's property search by address. 

Properties Add Page : 

Clicking on Add a Property button will ask the user for 

Name - 45 characters limit
Address - Mandatory 
Property Value - Mandatory 
Mortgage Value - Optional
Image for Property - Max 6 MB 

Every Property will have a tenancy period. 

Clicking on a tenancy period will allow user to add a start date, end date and the monthly rent for this period. 

A property cannot have a tenancy period overlapping with another tenancy period. Ex : 

--
In front of all properties, there will be a Add a Tenancy Period Button. Clicking on that will show a form, which allows start date, end date, monthly rent. Also at bottom should be an option to 

1) Add a new tenant. 
2) Choose an existing tenant 



We can add tenants to a tenancy period. 
