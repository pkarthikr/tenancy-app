@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Assign a tenancy</div>

                <div class="panel-body">
                      <div class="portlet-body form">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
    @if(count($tenants) > 0)
        <form role="form" action="{{url('/tenancy/done')}}" method="POST">
            {!!csrf_field()!!}
            <div class="form-body">
               @foreach($tenants as $tenant)
                  <input type="checkbox" value="{{$tenant->id}}" name="tenant[]">{{ $tenant->name }}
                  <br />
               @endforeach
            </div>
            <input type="hidden" value={{Session::get('tenancy_id')}} name="tenancy">
            <div class="form-actions">
                <button type="submit" name="add" class="btn blue">Assign Tenancy</button>
                 </div>  
        </form>
    @else 
    <div>You have no tenants in your system. Please <a href="/tenants/create">add a few tenants</a> and access this page from properties.</div>
    @endif
            </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
