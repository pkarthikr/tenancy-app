@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add a tenancy for Property Name</div>

                <div class="panel-body">
                      <div class="portlet-body form">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
        <form role="form" action="{{url('/tenancy/add')}}" method="POST">
            {!!csrf_field()!!}
            <div class="form-body">
                <div class="form-group">
                    <label>Start Date</label>
                    <div class="input-group">
                       <input type="date" class="form-control" placeholder="Enter tenancy start date" name="start_date" required> 
                    </div>
                </div>
                  <div class="form-group">
                    <label>End Date</label>
                    <div class="input-group">
                       <input type="date" class="form-control" placeholder="Enter tenancy end date" name="end_date" required> 
                    </div>
                </div>
                  <div class="form-group">
                    <label>Monthly Rent</label>
                    <div class="input-group">
                       <input type="text" class="form-control" placeholder="Enter monthly rent" name="monthly_rent" required> 
                    </div>
                </div>
                <input type="hidden" value= {{ $id }} name="property_id">
             
            </div>
            <div class="form-actions">
                <button type="submit" name="add" class="btn blue">Add Tenancy</button>
                 </div>  
              </form>
            </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
