@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Properties Dashboard</div>

                <div class="panel-body">
                      <div class="portlet-body form">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
        <form role="form" action="{{url('/properties/add')}}" enctype="multipart/form-data" method="POST">
            {!!csrf_field()!!}
            <div class="form-body">
                <div class="form-group">
                    <label>Property Name</label>
                    <div class="input-group">
                       <input type="text" class="form-control" placeholder="Enter your property name" name="name" required> 
                    </div>
                </div>
                 <div class="form-group">
                    <label>Property Address</label>
                    <div class="input-group">
                       <input type="text" class="form-control" placeholder="Enter your address" name="address" required> 
                    </div>
                </div>
                 <div class="form-group">
                    <label>Property Value</label>
                    <div class="input-group">
                       <input type="number" class="form-control" placeholder="Enter your property value" name="property_value" required> 
                    </div>
                </div>
                 <div class="form-group">
                    <label>Mortgage Value</label>
                    <div class="input-group">
                       <input type="number" class="form-control" placeholder="Enter your morgage value" name="mortgage_value" required> 
                    </div>
                </div>
                <div class="form-group">
                    <input type="file" name="image">
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" name="add" class="btn blue">Add Property</button>
                 </div>  
              </form>
            </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
