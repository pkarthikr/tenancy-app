@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Properties Dashboard</div>
                <a href="{{url('/properties/create')}}" class="btn btn-default">Add a Property</a>
                {{ Session::get('alert-success') }}
                <div class="panel-body">
                      @foreach($properties as $property)
                        {{$property->name}} <a href="/tenancy/create/{{$property->id}}">Add Tenancy Period</a> <a href="{{url('properties/'.$property->id)}}">View</a>
                        <br/>
                      @endforeach
                      {{ $properties->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
