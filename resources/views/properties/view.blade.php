@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Property Details</div>
                <div class="panel-body">
                
                 {{ $property->name }}

                      @foreach($periodData as $period)
                        {{$period['start_date']}} {{ $period['end_date']}} {{ $period['count']}} <a href="/tenancy/assign/{{$period['id']}}">Assign</a>
                        <br/>
                      @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
