@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add Tenants</div>

                <div class="panel-body">
                      <div class="portlet-body form">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
        <form role="form" action="{{url('/tenants/add')}}" enctype="multipart/form-data" method="POST">
            {!!csrf_field()!!}
            <div class="form-body">
                <div class="form-group">
                    <label>Name</label>
                    <div class="input-group">
                       <input type="text" class="form-control" placeholder="Enter your name" name="name" required> 
                    </div>
                </div>
                <div class="form-group">
                    <label>Address</label>
                    <div class="input-group">
                       <input type="text" class="form-control" placeholder="Enter your address" name="address" required> 
                    </div>
                </div>
                 <div class="form-group">
                    <label>User's Image</label>
                    <input type="file" name="image">
                </div>
             
            </div>
            <div class="form-actions">
                <button type="submit" name="add" class="btn blue">Add Tenant</button>
                 </div>  
              </form>
            </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
