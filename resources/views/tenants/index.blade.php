@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Tenants Dashboard</div>
<a href="{{url('/tenants/create')}}" class="btn btn-default">Add a Tenant</a>
                <div class="panel-body">
                      @foreach($tenants as $tenant)
                        {{$tenant->name}}
                      @endforeach
                      {{ $tenants->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
